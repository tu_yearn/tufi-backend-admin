module.exports = ({ env }) => ({
  host: env("HOST", "0.0.0.0"),
  port: env.int("PORT", 4500),
  admin: {
    auth: {
      secret: env("ADMIN_JWT_SECRET", "97d6f76616b564e3b1004a4c6c7f5997"),
    },
  },
});
